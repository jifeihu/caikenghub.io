---
layout: post
title:  idea debug 卡顿
categories: 
    - Brandon
tags: 
    - idea
    - jetbrains
prism: true
---

__问题描述:__

使用 IntelliJ IDEA Debug 代码时，经常在"Finishd, saving caches" 卡上很长时间。


__原因:__

	不明

__解决步骤:__

编辑 hosts 文件，在 localhost 后面加上主机名

 ```txt
 127.0.0.1 localhost <your hostname>
 ::1 localhost <your hostname>
 ```

__参考链接：__

https://stackoverflow.com/questions/20658400/intellij-idea-hangs-while-finished-saving-caches
