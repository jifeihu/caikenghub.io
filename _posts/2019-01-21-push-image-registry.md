---
layout: post
title:  上传镜像到私有仓库
categories: 
    - Holmes
tags: 
    - docker
    - registry
prism: true
---

__问题描述:__

​	通过命令行docker push 192.168.215.200:5000/java:latest将镜像推送到另一台主机搭建的私有仓库中。

```shell
Get https://192.168.215.200:5000/v1/_ping: http: server gave HTTP response to HTTPS client
```

__原因:__
	为了保证安全，跨机的镜像推送操作默认采用的都是https协议。客户端采用https，而docker registry未采用https服务所致。

__解决步骤:__
1. insecure registry配置
  在文件/etc/docker/daemon.json中添加一下内容

   ```shell
       {
           "insecure-registries":[
               "192.168.215.200:5000"
           ]
       }
   ```
2. secure registry
[喜欢我就戳我](https://www.cnblogs.com/xcloudbiz/articles/5526262.html)

