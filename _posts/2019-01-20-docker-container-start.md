---
layout: post
title:  启动docker容器
categories: 
    - Holmes
tags: 
    - docker
prism: true
---

__问题描述:__
	重启Linux系统，发现某个容器停止，然后通过命令docker start 2c36e8c5614(containerId)重新启动容器，报错信息如下。

```shell
Error response from daemon：Cannot start container 2c36e8c5614：[8]System error：Unit 2c36eb8c56148b2ee2ec2e42c0f2c690d561ab1ac9773920bada4ffde064faf3.scope already exists
```

__原因:__
	容器异常退出。

__解决步骤:__

```shell
1、systemctl stop docker-2c36eb8c56148b2ee2ec2e42c0f2c690d561ab1ac9773920bada4ffde064faf3.scope
2、docker start 2c36eb8c5614
```