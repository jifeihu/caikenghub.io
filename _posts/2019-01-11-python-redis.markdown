---
layout: post
title:  python redis setex指令 执行不成功
categories: 
    - Brandon
tags: 
    - Python
    - Redis
prism: true
---

__问题描述:__ Python 项目，调用 `redis.setex` 函数设置缓存后，通过 `redis-cli` 查看，发现并没有设置成功。

__解决步骤:__
1. 查看 db 配置

   发现 db 使用的默认配置，也就是 0

2. 再次通过 redis-cli 查看

	```shell
	127.0.0.1:6379[1]> select 0
	127.0.0.1:6379> keys *
	```

   还是没有。

3. 查看`setex`函数，函数定义如下：

   ```python
   def setex(self, name, time, value)
   ```

4. 查看调用代码:

   ```python
   r.setex(key, value, expire)
   ```

   发现，参数顺序写反了，改正：

   ```python
   r.setex(key, expire, value)
   ```

   改正之后，设置成功。

__总结__

- 参数写反了，执行却返回 `True` , 没有抛出异常。

- 使用 `Spring` 写多了项目，习惯了下面的参数顺序:

  ```java
  void set(K key, V value, long timeout, TimeUnit unit);
  ```

- Python redis 函数中的参数顺序与 `redis-cli` 中的参数顺序一致。