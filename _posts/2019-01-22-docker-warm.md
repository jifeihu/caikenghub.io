---
layout: post
title:  docker swarm加入集群失败
categories: 
    - Holmes
tags: 
    - docker
    - swarm
prism: true
---

__问题描述:__

​	通过命令docker swarm join加入到docker集群中的manager节点。

```shell
Error response from daemon: error while validating Root CA Certificate: x509: certificate has expired or is not yet valid
```

__原因:__
	docker swarm node之间是加密传输，docker swarm init时会创建CA证书。这里提示证书过期，可能是主机之间当前时间不一致。

__解决步骤:__

1. 通过date命令查看各个主机的当前时间，发现时间不一致；
2. 在各个主机上执行命令ntpdate cn.pool.ntp.org&& hwclock --systohc同步当前系统时间。如果ntpdate命令未找到，执行命令yum install ntp进行安装；
3. 在manager节点上执行命令docker swarm leave manager离开集群，docker swarm init重新初始化集群，然后worker节点加入集群。因为时间改变，证书失效，重新初始化生成证书；

